(ns metis.util
    (:gen-class))

(defn uuid [& not-important]
    (.toString (java.util.UUID/randomUUID)))
