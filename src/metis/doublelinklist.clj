(ns metis.doublelinklist
    (:gen-class))


(defn add-item-to-empty-dll-unsafe [dll itm]
    (assoc dll 0 (assoc itm :next nil :previous nil)))

(defn fix-previous [dll itm]
    (let [previous (get dll (:previous itm))]
        (if (not (nil? previous))
            (assoc dll (:id previous) (assoc previous :next (:id itm)))
            dll)))

(defn fix-next [dll itm]
    (let [next (get dll (:next itm))]
        (if (not (nil? next))
            (assoc dll (:id next) (assoc next :previous (:id itm)))
            dll)))

(defn- add-item-to-dll-unsafe [dll itm create-id]
    (let [new-id (create-id dll)
          itm (assoc itm :next (:next itm) :previous (:previous itm) :id new-id)
          dll (fix-previous dll itm)
          dll (fix-next dll itm)
          dll (assoc dll new-id itm)]
        dll))

(defn- can-add-item-to-dll-next [dll itm]
    (let [next (get dll (:next itm))
          previous (get dll (:previous itm))]
        (if (not (nil? next))
            (or (= (:previous next) (:id previous)) (nil? (:previous next)))
            true)))

(defn- can-add-item-to-dll-previous [dll itm]
    (let [next (get dll (:next itm))
          previous (get dll (:previous itm))]
        (if (not (nil? previous))
            (or (= (:next previous) (:id next)) (nil? (:next previous)) )
            true)))

(defn- next-and-previous-are-valid-nodes [dll itm]
    (and (or (nil? (:next itm)) (contains? dll (:next itm)))
         (or (nil? (:previous itm)) (contains? dll (:previous itm)))))

(defn- is-isolated-node [dll itm]
     (and (> (count dll) 0) (and (nil? (:next itm)) (nil? (:previous itm)))))

(defn- can-add-item-to-dll [dll itm]
    (and (next-and-previous-are-valid-nodes dll itm)
         (not (is-isolated-node dll itm))
         (can-add-item-to-dll-previous dll itm)
         (can-add-item-to-dll-next dll itm)))

(defn add-item-to-dll [dll itm create-id]
    (if (can-add-item-to-dll dll itm)
        (add-item-to-dll-unsafe dll itm create-id)
        dll))

(defn get-item-by-previous[dll previous-id]
    (let [previous-node (get dll previous-id)
          target-id (:next previous-node)]
        (get dll target-id)))

(defn get-item-by-next[dll next-id]
    (let [next-node (get dll next-id)
          target-id (:previous next-node)]
        (get dll target-id)))

(defn- get-item-by-previous-and-next-unsafe [dll previous-id next-id]
    (cond
        (and (nil? previous-id) (nil? next-id)) (get (first dll) 1)
        (nil? previous-id) (get-item-by-next dll next-id)
        :else (get-item-by-previous dll previous-id)))

(defn get-item-by-previous-and-next
    ([dll previous-id]
        (get-item-by-previous-and-next dll previous-id nil))
    ([dll previous-id next-id]
        (let [proposal (get-item-by-previous-and-next-unsafe dll previous-id next-id)]
            (if (and (= (:next proposal) next-id) (= (:previous proposal) previous-id))
                proposal
                nil))))
