(ns metis.web
    (:require [compojure.core :as compojure])
    (:require [ring.adapter.jetty :as jetty])
    (:require [cheshire.core :as cheshire])
    (:require [metis.util :as util])
    (:require [metis.doublelinklist :refer :all])
    (:gen-class))

(defn jsonify-response [response]
    {:body (cheshire/generate-string response)
     :headers {"Content-Type" "application/json"}
     :status 200})

(defn jsonify-handler [handler]
    (fn [request]
        (jsonify-response (handler request))))

(defn arrayify-dll [key-value]
    (let [key (get key-value 0)
          value (get key-value 1)]
          (assoc value :id key)))

(defn create-playlist-get-handler [playlist-atom]
    (fn [request]
        (map arrayify-dll (seq @playlist-atom))))

(defn get-trak-from-request [request]
    (cheshire/parse-stream  (clojure.java.io/reader (:body request)) true))


(defn add-trak-to-playlist [playlist-atom track]
    (swap! playlist-atom (fn [playlist]
        (add-item-to-dll playlist track util/uuid))))

(defn- get-track-in-playlist [playlist track]
    (get-item-by-previous-and-next playlist (:previous track) (:next track)))


(defn create-playlist-post-handler [playlist-atom]
    (fn [request]
        (let [track (get-trak-from-request request)
             play-list (add-trak-to-playlist playlist-atom track)
             new-track (get-track-in-playlist play-list track)]
            new-track)))

(defn create-track-get-handler [playlist-atom]
    (fn [request]
        (let [playlist @playlist-atom
              track-id (get-in request [:params :id])]
            (get playlist track-id))))

(defn get-playlist-handles []
    (let [playlist (atom {})]
        (list (compojure/GET "/playlist" [] (jsonify-handler (create-playlist-get-handler playlist)))
              (compojure/POST "/playlist" [] (jsonify-handler (create-playlist-post-handler playlist)))
              (compojure/GET "/playlist/:id" [id] (jsonify-handler (create-track-get-handler playlist)))
            ;   (compojure/DELETE "/playlist/:id" [id] (create-track-delete-handler playlist))
              )))

(defn create-player-get-handler [player-head]
    (fn [request]
        (let [public-keys [:track-id]]
            (select-keys @player-head public-keys))))

(defn- send-stop-signal [player-head-atom]
    (swap! player-head-atom (fn [player-head]
        (assoc player-head :stop-signal true))))

(defn wait-track-finish [player-head-atom]
    (let [track-future (:track-future @player-head-atom)]
        (if (future? track-future)
            @track-future)))

(defn cancel-stop-signal [player-head-atom]
    (swap! player-head-atom (fn [player-head]
        (assoc player-head :stop-signal false))))

(defn stop-current-track [player-head-atom]
    (send-stop-signal player-head-atom)
    (wait-track-finish player-head-atom)
    (cancel-stop-signal player-head-atom))

(defn play-next [track player-head playlist]
    )

(defn create-player-post-handler [playlist-atom player-head-atom]
    (fn [request]
        (let [track 12]
            (stop-current-track player-head-atom)
            ; (play-track track player-head-atom playlist-atom)
            )))

(defn get-player-handlers []
    (let [playlist (atom {})
          player-head (atom {})]
        (list
            (compojure/GET "/player" [] (jsonify-handler (create-player-get-handler player-head))))))



(defn get-playlist-routes []
    (let [routes (get-playlist-handles)]
        (apply compojure/routes routes)))

(defn -main [& args]
    (jetty/run-jetty (get-playlist-routes) {:port 3000}))
