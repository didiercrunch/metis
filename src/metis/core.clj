(ns metis.core
    (:require [metis.web :as web])
    (:require [metis.player :as player])
    (:gen-class))

(defn help [args]
    (println "Metis"))

(defn -main [& args]
    (let [sub-command (first args)
          sub-args (rest args)]
        (case sub-command
            "web" (apply web/-main sub-args)
            "player" (apply player/-main sub-args)
            (help sub-args))))
