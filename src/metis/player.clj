(ns metis.player
    (:import [javax.sound.sampled AudioSystem AudioFormat])
    (:import javax.sound.sampled.AudioFormat$Encoding)
    (:import javax.sound.sampled.SourceDataLine)
    (:import javax.sound.sampled.DataLine$Info)
    (:require [clj-http.client :as client])
    (:gen-class))


(defn- stream-audio [input output player-head-atom]
    (let [buffer (byte-array 65536)]
        ((fn [buffer n]
            (.write output buffer 0 n)
            (let [n (.read input buffer)]
                (when (and (not (= n -1)) (not (get @player-head-atom :stop-signal)))
                    (recur buffer n)
                )
            )) buffer 0)  ))


(defn- play-input-in-ouput [output-line input out-format player-head-atom]
    (.open output-line out-format)
    (.start output-line)
    (stream-audio (AudioSystem/getAudioInputStream out-format input) output-line player-head-atom)
    (.drain output-line)
    (.stop output-line)
    )


(defn- get-output-format [audio-format]
    (let [ch (.getChannels audio-format)
          rate (.getSampleRate audio-format)
          pcm_signed AudioFormat$Encoding/PCM_SIGNED]
        (AudioFormat. pcm_signed rate 16 ch (* 2 ch) rate false)))


(defn- play-input-stream [input-stream player-head-atom]
    (let [in (AudioSystem/getAudioInputStream input-stream)
          output-format (get-output-format (.getFormat in))
          info (DataLine$Info. SourceDataLine output-format)
          line (AudioSystem/getLine info)]
        (play-input-in-ouput line in output-format player-head-atom)))


(defn play-url [filename player-head-atom]
    (let [input-stream (clojure.java.io/input-stream filename)]
        (play-input-stream input-stream player-head-atom)))


(defn -main [filename]
    (let [player-head-atom (atom {})]
        (play-url filename player-head-atom)))
