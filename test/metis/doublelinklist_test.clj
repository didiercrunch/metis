(ns metis.doublelinklist-test
  (:require [clojure.test :refer :all]
            [metis.doublelinklist :refer :all]))


(deftest test-add-item-to-dll
    (defn create-id [dll]
        (count dll))

    (testing "add first element in double linked list"
        (let [dll {}
              complexe-itm {:next nil :previous nil :foo "bar"}
              simple-itm {:foo "bar"}
              expt {0 {:next nil :previous nil :foo "bar" :id 0}}]
            (is (= (add-item-to-dll dll complexe-itm create-id) expt))
            (is (= (add-item-to-dll dll simple-itm create-id) expt))))

    (testing "add to the end"
        (let [dll (add-item-to-dll {} {:foo "bar"} create-id)
              itm {:previous 0 :foo "toto"}
              expt {0 {:next 1 :previous nil :foo "bar" :id 0} 1 {:next nil :previous 0 :foo "toto" :id 1}}]
            (is (= (add-item-to-dll dll itm create-id) expt))))

    (testing "add to the beginning"
        (let [dll (add-item-to-dll {} {:foo "bar"} create-id)
              itm {:next 0 :foo "toto"}
              expt {0 {:next nil :previous 1 :foo "bar" :id 0} 1 {:next 0 :previous nil :foo "toto" :id 1}}]
            (is (= (add-item-to-dll dll itm create-id) expt))))

    (testing "add to the middle"
        (let [dll (add-item-to-dll {} {:foo "bar"} create-id)
              dll (add-item-to-dll dll {:previous 0 :foo "toto"} create-id)
              itm {:previous 0 :next 1 :foo "caca"}
              expt {0 {:next 2 :previous nil :foo "bar" :id 0}
                    2 {:next 1 :previous 0 :foo "caca" :id 2}
                    1 {:next nil :previous 2 :foo "toto" :id 1}}]
            (is (= (add-item-to-dll dll itm create-id) expt))))

    (testing "close the double link list"
        (let [dll (add-item-to-dll {} {:foo "bar"} create-id)
              dll (add-item-to-dll dll {:previous 0 :foo "toto"} create-id)
              itm {:previous 1 :next 0 :foo "caca"}
              expt {0 {:next 1 :previous 2 :foo "bar" :id 0}
                    1 {:next 2 :previous 0 :foo "toto" :id 1}
                    2 {:next 0 :previous 1 :foo "caca" :id 2}
                    }]
            (is (= (add-item-to-dll dll itm create-id) expt))))

    (testing "cannot add a stupid nodes in double link list"
        (let [dll (add-item-to-dll {} {:foo "bar"} create-id)
              dll (add-item-to-dll dll {:previous 0 :foo "caca"} create-id)
              stupid-itms [{:next nil :previous nil :foo "toto"}
                           {:next 45 :previous nil :foo "toto"}
                           {:next nil :previous 66 :foo "toto"}
                           {:next 77 :previous 66 :foo "toto"}
                           {:next nil :previous 0 :foo "toto"}
                           {:next 1 :previous nil :foo "toto"}]
              verify (fn [stupid-itm]
                         (is (= (add-item-to-dll dll stupid-itm create-id) dll)))
              ]
              (doall (map verify stupid-itms)))))


(deftest test-get-item-by-previous-and-next
    (defn create-id [dll]
        (count dll))

    (testing "get valid element"
        (let [dll (add-item-to-dll {} {:foo "0"} create-id)
              dll (add-item-to-dll dll {:foo "1" :previous 0} create-id)
              dll (add-item-to-dll dll {:foo "2" :previous 1} create-id)]
            (is (= (get-item-by-previous-and-next dll 0 2) (get dll 1)))
            (is (= (get-item-by-previous-and-next dll 1 nil) (get dll 2)))
            (is (= (get-item-by-previous-and-next dll 1) (get dll 2)))
            (is (= (get-item-by-previous-and-next dll nil 1) (get dll 0)))))

    (testing "get invalid element"
        (let [dll (add-item-to-dll {} {:foo "0"} create-id)
              dll (add-item-to-dll dll {:foo "1" :previous 0} create-id)
              dll (add-item-to-dll dll {:foo "2" :previous 1} create-id)]
            (is (= (get-item-by-previous-and-next dll 2 0) nil))
            (is (= (get-item-by-previous-and-next dll 1 2) nil))
            (is (= (get-item-by-previous-and-next dll 0) nil))))

    (testing "get elements from trivial dll"
        (let [empty-dll {}
              single-dll (add-item-to-dll {} {:foo "0"} create-id)]
            (is (= (get-item-by-previous-and-next empty-dll 2 0) nil))
            (is (= (get-item-by-previous-and-next empty-dll nil nil) nil))
            (is (= (get-item-by-previous-and-next single-dll nil nil) (get single-dll 0)))
            )))
