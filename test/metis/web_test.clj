(ns metis.web-test
  (:require [clojure.test :refer :all]
            [metis.web :refer :all]
            [cheshire.core :as cheshire]))


(deftest test-create-playlist-get-handler

    (testing "empty playlist"
        (let [playlist-atom (atom {})
              handler (create-playlist-get-handler playlist-atom)]
            (is (= (handler {}) []))))

    (testing "single element playlist"
        (let [playlist-atom (atom {1 {:next nil :previous nil :foo "toto"}})
              handler (create-playlist-get-handler playlist-atom)]
            (is (= (handler {}) [{:id 1 :next nil :previous nil :foo "toto"}]))))

    (testing "double element playlist"
        (let [playlist-atom (atom {1 {:next 2 :previous nil :foo "toto"}
                                   2 {:next nil :previous 1 :foo "coco"}})
              handler (create-playlist-get-handler playlist-atom)]
            (is (= (handler {}) '({:id 1 :next 2 :previous nil :foo "toto"}
                                  {:id 2 :next nil :previous 1 :foo "coco"}))))))


(deftest test-create-playlist-post-handler
    (defn string->stream
        ([s] (string->stream s "UTF-8"))
        ([s encoding]
            (-> s
                (.getBytes encoding)
                (java.io.ByteArrayInputStream.))))

    (defn mock-request [body]
        {:body (string->stream (cheshire/generate-string body))})

    (testing "adding first track"
        (let [playlist-atom (atom {})
             handler (create-playlist-post-handler playlist-atom)
             request (mock-request {:foo "bar"})
             response (handler request)]
            (is (= (count @playlist-atom) 1))
            )))
