(defproject metis "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.8.0"]

    ;; all dependencies to read music
    [com.googlecode.soundlibs/tritonus-share "0.3.7-3"]
    [com.googlecode.soundlibs/tritonus-all "0.3.7-1"]
    [com.googlecode.soundlibs/vorbisspi "1.0.3-2"]
    [com.googlecode.soundlibs/jorbis "0.0.17-3"]
    [com.googlecode.soundlibs/jlayer "1.0.1-2"]
    [com.googlecode.soundlibs/mp3spi "1.9.5-2"]

    ;; web dependencies
    [ring/ring-core "1.4.0"]
    [ring/ring-jetty-adapter "1.4.0"]
    [ring/ring-json "0.4.0"]
    [compojure "1.5.0"]

    ;; other dependencies
    [cheshire "5.6.1"]
    [clj-http "3.1.0"]

    ;; dev dependencies
    [lein-cloverage "1.0.6"]
    ]
  :plugins [[lein-ring "0.9.7"]
            [lein-cloverage "1.0.0"]]
  :main ^:skip-aot metis.core
  :ring {:handler metis.core/handler}
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
